//
//  DummyCell.h
//  demotest
//
//  Created by thanh tung  on 5/1/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DummyCell : UITableViewCell
@property(weak,nonatomic) IBOutlet UILabel *lblMessage;

@property(weak,nonatomic) IBOutlet UIView *viewRoot;
@property (weak, nonatomic) IBOutlet UIImageView *avatarUser;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblLike;
@end
