//
//  ListViewController.m
//  demotest
//
//  Created by thanh tung  on 4/30/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "ListViewController.h"
#import "CarbonKit.h"
#import "Home/HomeViewController.h"
#import "DummyCell.h"
#import "HexColors.h"
#import <MBProgressHUD.h>
#import "SVList.h"
#import "Manager/Manager.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"

@interface ListViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *items;
}
@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Home";
    [Manager instance].listVC = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    SVList *svList = [[SVList alloc] init];
    [svList getDataFromServer];
    UIBarButtonItem *btnRight=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Setting.png"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:nil];
        self.navigationItem.rightBarButtonItem=btnRight;
}
-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifierHome = @"DummyCell";
    DummyCell *cell = (DummyCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifierHome];
    if (cell==nil) {
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"DummyCell" owner:nil options:nil];
        for (id obj in nibArray) {
            if ([obj isMemberOfClass:[DummyCell class]]) {
                cell = (DummyCell *)obj;
            }
        }
    }
    NSString * avatarURL;
    avatarURL  = self.arListDummy[indexPath.row][@"consultation"][@"thumbnail_urls"][@"default"];
    if ([avatarURL isEqual:[NSNull null]]) {
        avatarURL = @"";
    }
    if (!avatarURL) {
        avatarURL = @"";
    }
    NSString *name = self.arListDummy[indexPath.row][@"consultation"][@"nickname"];
    cell.lblName.text = name;
    UIImage * defaultImg;
    if (name.length>2) {
        NSString * stringAvatar = [[Manager instance]getStrLetterWithName:name];
        defaultImg = [[Manager instance] getAvatar:stringAvatar];
    }
    [cell.avatarUser sd_setImageWithURL:[NSURL URLWithString:avatarURL] placeholderImage:defaultImg completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
         cell.avatarUser.image = [[Manager instance] imageRounded:image withSize:cell.avatarUser.frame.size];
        }
    }];
    NSString *msg = self.arListDummy[indexPath.row][@"consultation"][@"description"];
    cell.lblMessage.text = msg;
    cell.viewRoot.layer.borderColor = [UIColor colorWithHexString:@"#cccccc"].CGColor;
    cell.viewRoot.layer.borderWidth =1;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 158;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arListDummy count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    DetailViewController *detailVC =  [main instantiateViewControllerWithIdentifier:@"DetailViewController"];
    if ([self.arListDummy[indexPath.row] count] >0) {
       detailVC.dic = [[NSDictionary alloc] initWithDictionary:self.arListDummy[indexPath.row]];
       [self.navigationController pushViewController:detailVC animated:YES];
    }
    

}




@end
