//
//  Manager.m
//  demotest
//
//  Created by thanh tung  on 4/30/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "Manager.h"
#import <MBProgressHUD.h>
@implementation Manager
- (id) init {
    self = [super init];
    if (self != nil) {
        
    }
    return self;
}

static Manager *gInstanceManager;

+ (Manager*)instance {
    @synchronized(self) {
        if (!gInstanceManager){
            gInstanceManager = [[self alloc] init];
        }
    }
    
    return gInstanceManager;
}

-(UIImage *) getImageAvatarLetter: (CGRect) frame withString: (NSString *) text withColor:(UIColor *)color{
    //
    // Set up a temporary view to contain the text label
    //
    UIView *tempView = [[UIView alloc] initWithFrame:frame];
    
    UILabel *letterLabel = [[UILabel alloc] initWithFrame:frame];
    letterLabel.textAlignment = NSTextAlignmentCenter;
    letterLabel.backgroundColor = [UIColor clearColor];
    letterLabel.textColor = [UIColor whiteColor];
    letterLabel.adjustsFontSizeToFitWidth = YES;
    letterLabel.minimumScaleFactor = 8.0f / 65.0f;
    letterLabel.font = [UIFont systemFontOfSize:CGRectGetWidth(frame) * 0.48];;
    [tempView addSubview:letterLabel];
    
    letterLabel.text = text;
    
    //
    // Set the background color
    //
    //    tempView.backgroundColor = color ? color : [self randomColor:text];
    tempView.backgroundColor = [self randomGradientColor:text];
    //
    // Return an image instance of the temporary view
    //
    CGFloat scale = [UIScreen mainScreen].scale;
    CGSize size = frame.size;
    
    UIGraphicsBeginImageContextWithOptions(size, NO, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddPath(UIGraphicsGetCurrentContext(),
                     [UIBezierPath bezierPathWithRoundedRect:frame cornerRadius:size.width].CGPath);
    CGContextTranslateCTM(context, -frame.origin.x, -frame.origin.y);
    CGContextClip(UIGraphicsGetCurrentContext());
    
    [tempView.layer renderInContext:context];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return snapshot;
}
typedef struct
{
    double r;       // percent [0 - 1]
    double g;       // percent [0 - 1]
    double b;       // percent [0 - 1]
    double a;       // percent [0 - 1]
} RGBA;

typedef struct
{
    double h;       // angle in degrees [0 - 360]
    double s;       // percent [0 - 1]
    double v;       // percent [0 - 1]
} HSV;
- (UIColor *)randomGradientColor:(NSString *)inputString {
    float golden_ratio = 0.618033988749895f;
    float radius = 0.1f;
    float shift = 0.05f;
    float offset;
    if (inputString.length == 1) {
        offset = [inputString characterAtIndex:0];
        offset *= golden_ratio;
    }
    else if (inputString.length > 1) {
        offset = ([inputString characterAtIndex:0] + [inputString characterAtIndex:1]);
        offset *= golden_ratio;
    }
    else {
        offset = drand48()*100;
    }
    
    offset = fmodf((offset + golden_ratio), 1.0);
    offset = floor(offset/radius) * radius;
    offset += shift;
    
    HSV resultHSV1, resultHSV2;
    resultHSV1.h = offset;
    resultHSV1.s = 1.0;
    resultHSV1.v = 0.7;
    resultHSV2.h = resultHSV1.h;
    resultHSV2.s = resultHSV1.s;
    resultHSV2.v = resultHSV1.v + 0.15;
    UIColor *resultColor1;
    
    resultColor1 = [UIColor colorWithHue:resultHSV1.h saturation:resultHSV1.s brightness:resultHSV1.v alpha:0.75];
    
    return resultColor1;
}
- (UIImage *) getAvatar:(NSString *)str{
    UIImage * avatar = self.dic[str];
    if (avatar) {
        return avatar;
    }
    else{
        avatar = [self getImageAvatarLetter:CGRectMake(0, 0, 50, 50) withString:str withColor:nil];
        self.dic[str] = avatar;
        return avatar;
    }
}
-(NSString *) getStrLetterWithName: (NSString *) text{
    
    NSMutableString *displayString = [NSMutableString stringWithString:@""];
    
    NSArray *words = [text componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([words count]) {
        NSString *firstWord = words[0];
        if ([firstWord length]) {
            [displayString appendString:[firstWord substringWithRange:NSMakeRange(0, 1)]];
        }
        
        if ([words count] >= 2) {
            NSString *lastWord = words[[words count] - 1];
            if ([lastWord length]) {
                [displayString appendString:[lastWord substringWithRange:NSMakeRange(0, 1)]];
            }
        }
    }
    return [displayString uppercaseString];
}


-(void) showToastWithString:(NSString *)text withView:(UIView *)view{
    [self showToastWithString:text withView:view autoHide:YES];
}

-(void) showToastWithString:(NSString *)text withView:(UIView *)view autoHide:(BOOL)autoHide{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.opaque = YES;
    hud.opacity = 0.75f;
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"";
    hud.detailsLabelFont = [UIFont fontWithName:@"HelveticaNeue" size:15.0];
    hud.detailsLabelText = text;
    hud.margin = 15.f;
    hud.removeFromSuperViewOnHide = YES;
    if (autoHide) {
        [hud hide:YES afterDelay:1.5f];
    }
}
- (UIImage *) imageRounded: (UIImage *) image withSize:(CGSize) size{
    CGRect rect = (CGRect){0.f, 0.f, size};
    
    UIGraphicsBeginImageContextWithOptions(size, NO, UIScreen.mainScreen.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddPath(UIGraphicsGetCurrentContext(),
                     [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:size.width].CGPath);
    CGContextClip(context);
    
    [image drawInRect:rect];
    UIImage *output = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return output;
}

@end
