//
//  Manager.h
//  demotest
//
//  Created by thanh tung  on 4/30/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainVC.h"
#import "ListViewController.h"

@interface Manager : NSObject
+ (Manager*)instance;
@property (nonatomic, strong) MainVC * mainVC;
@property(weak,nonatomic) ListViewController *listVC;
@property (nonatomic, strong) NSMutableDictionary * dic;
- (UIImage *) getAvatar: (NSString *) str;
-(NSString *) getStrLetterWithName: (NSString *) text;
-(void) showToastWithString:(NSString *)text withView:(UIView *)view;
-(void) showToastWithString:(NSString *)text withView:(UIView *)view autoHide:(BOOL)autoHide;
- (UIImage *) imageRounded: (UIImage *) image withSize:(CGSize) size;
@end
