//
//  MainVC.h
//  demotest
//
//  Created by thanh tung  on 1/5/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainVC : UITabBarController
- (void) pushNewVC: (UIViewController *) vc animated: (BOOL) animated;
@end
