//
//  MainVC.m
//  demotest
//
//  Created by thanh tung on 1/5/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "MainVC.h"
#import "ViewController.h"
#import "Manager.h"
#import "ListViewController.h"
#import "HomeViewController.h"
#import "HexColors.h"
@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTapBar];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initTapBar{
    self.tabBar.translucent = NO;
    UIView * viewBg = [[UIView alloc] init];
    viewBg.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 64);
    viewBg.backgroundColor = [UIColor colorWithHexString:@"#ff6699" alpha:1];
    UIImage * navBg = [self imageWithView:viewBg];
    [[UINavigationBar appearance] setBackgroundImage:navBg forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackgroundImage:navBg forBarMetrics:UIBarMetricsLandscapePhone];
    
    {
                UIView * view = [[UIView alloc] init];
                view.frame = CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 1);
                view.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0];
                UIImage * img = [self imageWithView:view];
                [[UINavigationBar appearance] setShadowImage:img];
                [[UITabBar appearance] setShadowImage:img];
    }
    UINavigationController *nc1 =[[UINavigationController alloc] init];
    {
        HomeViewController *vc = [[HomeViewController alloc] initWithNibName:nil bundle:nil];
        
        nc1 = [[UINavigationController alloc] initWithRootViewController:vc];
        nc1.navigationBar.translucent = NO;
    }
    [nc1.tabBarItem setTitle:@"HOME"];
    [nc1.tabBarItem setImage:[UIImage imageNamed:@"homeTap.png"]];
    //tab 2
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ViewController *vc =  [main instantiateViewControllerWithIdentifier:@"ViewController"];
    UINavigationController *nc2 =[[UINavigationController alloc] init];;
    {
        nc2 = [[UINavigationController alloc] initWithRootViewController:vc];
        nc2.navigationBar.translucent = NO;
    }
    [nc2.tabBarItem setTitle:@"SEARCH"];
    [nc2.tabBarItem setImage:[UIImage imageNamed:@"search_icon.png"]];
    //tab 3
    
    UINavigationController *nc3 =[[UINavigationController alloc] init];;
    {
        
        nc3 = [[UINavigationController alloc] initWithRootViewController:vc];
        nc3.navigationBar.translucent = NO;
    }
    [nc3.tabBarItem setTitle:@"WRITING"];
    [nc3.tabBarItem setImage:[UIImage imageNamed:@"tap.png"]];
    //tab4
    UINavigationController *nc4 =[[UINavigationController alloc] init];;
    {
        
        nc4 = [[UINavigationController alloc] initWithRootViewController:vc];
        nc4.navigationBar.translucent = NO;
    }
    [nc4.tabBarItem setTitle:@"INFORMATION"];
    [nc4.tabBarItem setImage:[UIImage imageNamed:@"information.png"]];
    //tab5
    UINavigationController *nc5 =[[UINavigationController alloc] init];;
    {
        
        nc5 = [[UINavigationController alloc] initWithRootViewController:vc];
        nc5.navigationBar.translucent = NO;
    }
    [nc5.tabBarItem setTitle:@"MY PAGE"];
    [nc5.tabBarItem setImage:[UIImage imageNamed:@"mypage.png"]];
   self.viewControllers = @[nc1,nc2,nc3,nc4,nc5];
    [self.tabBar setTintColor:[UIColor colorWithHexString:@"#ff6699"]];
}
- (void) pushNewVC:(UIViewController *)vc animated:(BOOL)animated {
    UINavigationController * nav = [Manager instance].mainVC.viewControllers[[Manager instance].mainVC.selectedIndex];
    [nav pushViewController:vc animated:animated];
    
}

- (UIImage *) imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}
@end
