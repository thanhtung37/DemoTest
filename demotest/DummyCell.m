//
//  DummyCell.m
//  demotest
//
//  Created by thanh tung  on 5/1/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "DummyCell.h"

@implementation DummyCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
