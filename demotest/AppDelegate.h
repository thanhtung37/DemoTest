//
//  AppDelegate.h
//  demotest
//
//  Created by thanh tung  on 4/30/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

