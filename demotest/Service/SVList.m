//
//  SVList.m
//  demotest
//
//  Created by thanh tung  on 5/1/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "SVList.h"
#import "ListViewController.h"
#import "Manager.h"
#import <MBProgressHUD.h>
#define KListdummy @"http://www.json-generator.com/api/json/get/cfJoaXblBu?indent=2"
@implementation SVList

-(void)getDataFromServer{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:KListdummy parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([responseObject count]>0) {
            ListViewController *controller = [Manager instance].listVC;
            controller.arListDummy = [[NSMutableArray alloc] initWithArray:responseObject[@"cards"]];
            [controller.arListDummy removeObjectAtIndex:0];
            if (controller) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:controller.view animated:YES];
                    [controller.tableView reloadData];
                });
              
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [MBProgressHUD hideHUDForView:controller.view animated:YES];
                });
            }
            
        }
        
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        ListViewController *controller = [Manager instance].listVC;
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:controller.view animated:YES];
            [[Manager instance] showToastWithString:@"Kết nối không thành công!" withView:controller.view];
        });
        NSLog(@"errors");
    
    }];
}

@end
