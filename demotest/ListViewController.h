//
//  ListViewController.h
//  demotest
//
//  Created by thanh tung  on 4/30/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Home/HomeViewController.h"

@interface ListViewController : UIViewController

@property(weak,nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic) NSArray *array;
@property(strong,nonatomic) NSMutableArray *arListDummy;

@end
