//
//  DetailViewController.h
//  demotest
//
//  Created by thanh tung  on 5/3/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
@property(strong,nonatomic) NSDictionary *dic;
@property(weak,nonatomic) IBOutlet UILabel *lblName;
@property(weak,nonatomic) IBOutlet UIImageView *avatar;
@property(weak,nonatomic) IBOutlet UITextView *textviewDes;
@end
