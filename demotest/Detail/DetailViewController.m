//
//  DetailViewController.m
//  demotest
//
//  Created by thanh tung  on 5/3/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "DetailViewController.h"
#import "UIImageView+WebCache.h"
#import "Manager.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textviewDes.showsVerticalScrollIndicator = NO;
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    UIBarButtonItem *btnRight=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"logout.png"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:nil];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=btnRight;
    self.title = @"Detail";
    NSString *name =self.dic[@"consultation"][@"nickname"];
    self.lblName.text =name;
    self.textviewDes.text =self.dic[@"consultation"][@"description"];
    NSString * avatarURL;
    avatarURL  = self.dic[@"consultation"][@"thumbnail_urls"][@"default"];
    if ([avatarURL isEqual:[NSNull null]]) {
        avatarURL = @"";
    }
    if (!avatarURL) {
        avatarURL = @"";
    }
    UIImage * defaultImg;
    if (name.length>2) {
        NSString * stringAvatar = [[Manager instance]getStrLetterWithName:name];
        defaultImg = [[Manager instance] getAvatar:stringAvatar];
    }
    [self.avatar sd_setImageWithURL:[NSURL URLWithString:avatarURL] placeholderImage:defaultImg completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            self.avatar.image = [[Manager instance] imageRounded:image withSize:self.avatar.frame.size];
        }
    }];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
