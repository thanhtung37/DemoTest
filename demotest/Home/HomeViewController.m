//
//  HomeViewController.m
//  demotest
//
//  Created by thanh tung  on 5/1/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import "HomeViewController.h"
#import "CarbonKit.h"
#import "ListViewController.h"
#import "HexColors.h"

@interface HomeViewController ()<CarbonTabSwipeNavigationDelegate>
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem *btnRight=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Setting.png"]
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:nil];
    
    self.navigationItem.rightBarButtonItem=btnRight;
    items = @[@"Segment 1", @"Segment 2"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    [self style];
    
}


- (void)style {
    
    UIColor *color = [UIColor colorWithHexString:@"#ff6699"];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    [carbonTabSwipeNavigation setIndicatorColor:color];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    float wTap = [[UIScreen mainScreen] bounds].size.width/2;
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:wTap forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:wTap forSegmentAtIndex:1];
    
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont boldSystemFontOfSize:14]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    ListViewController *listVC = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    switch (index) {
            
        case 0:
            return listVC;
            
        case 1:
            return listVC;
            
        default:
            return listVC;
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    switch(index) {
        case 0:
            self.title = @"Home";
            break;
        case 1:
            self.title = @"Home";
            break;
        default:
            self.title = @"";
            break;
    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}


@end
