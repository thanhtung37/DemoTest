//
//  main.m
//  demotest
//
//  Created by thanh tung  on 4/30/16.
//  Copyright © 2016 thanh tung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
